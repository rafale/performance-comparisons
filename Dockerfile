# fastify, stric, Bun, Node, Qoper8-cp + YottaDB

# M/Gateway Developments Ltd
# 2 October 2023

FROM debian:bullseye

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y \
  curl \
  build-essential \
  make \
  gcc \
  git \
  gpg \
  wget \
  dos2unix \
  locate \
  nano \
  xinetd \
  libelf1 \
  libtinfo5 \
  lsb-release \
  lsof \
  libssl-dev \
  libicu-dev \
  locales \
  openssh-server \
  subversion \
  pkg-config \
  file \
  python3 \
  apache2 \
  apache2-utils \
  apache2-dev \
  cmake \
  unzip

RUN locale-gen "en_US.UTF-8"

# ===  Install Node.js & NPM

ENV NODE_VERSION 18.18.0

RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && set -ex \
  && for key in \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    141F07595B7B3FFE74309A937405533BE57C7D57 \
    74F12602B6F1C4E913FAA37AD3A89613643B6201 \
    DD792F5973C6DE52C432CBDAC77ABFA00DDBF2B7 \
    61FC681DFB92A079F1685E77973F295594EC4689 \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    890C08DB8579162FEE0DF9DB8BEAB4DFCF555EF4 \
    C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
    108F52B48DB57BB0CC439B2997B01419BD92F80A \
  ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  # smoke tests
  && node --version \
  && npm --version

# Install Bun
# RUN curl -fsSL https://bun.sh/install | bash \
RUN curl -fsSL https://bun.sh/install | bash -s -- bun-v1.0.4 \
  && export BUN_INSTALL="$HOME/.bun" \
  && export PATH="$BUN_INSTALL/bin:$PATH"

WORKDIR /opt/qoper8
RUN npm install qoper8-fastify qoper8-stric mg-dbx-napi redis

# Create app directory
RUN mkdir /opt/yottadb \
 && mkdir /opt/mg_web

COPY ./ydb /opt/qoper8
COPY ./ydb_run /opt/qoper8
COPY ./start /opt/qoper8
COPY ./stop /opt/qoper8
COPY ./reconfigure /opt/qoper8
COPY ./build_routes /opt/qoper8
COPY ./restart /opt/qoper8
COPY ./m/ /opt/qoper8/m/
COPY ./gde.txt /opt/qoper8
COPY ./benchmarks/ /opt/qoper8/benchmarks/

RUN echo "Installing YottaDB..."

RUN mkdir /tmp/tmp \
  && wget -P /tmp/tmp https://gitlab.com/YottaDB/DB/YDB/raw/master/sr_unix/ydbinstall.sh \
  && cd /tmp/tmp \
  && chmod +x ydbinstall.sh \
  && ./ydbinstall.sh --utf8 default --gui \
  && export ydb_gbldir=/opt/yottadb/yottadb.gld \
  && /usr/local/lib/yottadb/r138/mumps -run ^GDE < /opt/qoper8/gde.txt \
  && /usr/local/lib/yottadb/r138/mupip create \
  && /usr/local/lib/yottadb/r138/mupip extend -blocks=48000 DEFAULT

ENV ydb_gbldir "/opt/yottadb/yottadb.gld"
ENV ydb_routines "/opt/qoper8/m /usr/local/lib/yottadb/r138/plugin/o/_ydbmwebserver.so /usr/local/lib/yottadb/r138/libyottadbutil.so"
ENV ydb_dist "/usr/local/lib/yottadb/r138"
ENV PATH="${PATH}:/usr/local/lib/yottadb/r138"

# Install and configure the network mgsi interface code to allow
# network access to YottaDB (default setup uses
# API access to YottaDB)


# Install mg-web

RUN cd /opt \
 && git clone https://github.com/chrisemunt/mg_web  \
 && cp /opt/mg_web/src/*.h /opt/mg_web \
 && cp /opt/mg_web/src/*.c /opt/mg_web \
 && cp /opt/mg_web/src/apache/* /opt/mg_web \
 && cd /opt/mg_web \
 && dos2unix *

RUN cd /opt/mg_web \
 && apxs -a -i -c mg_web_apache.c mg_web.c mg_webstatus.c mg_webtls.c mg_websocket.c


# Fetch the generic mg_web server repository
# and move its M code and mg_web conf and log
# files into place

RUN cd /opt \
  && git clone https://github.com/robtweed/mgweb-server \
  && mv /opt/mgweb-server/m/* /opt/qoper8/m/ \
  && mv /opt/mgweb-server/config/mgweb.conf.json /opt/qoper8/mgweb.conf.json \
  && mv /opt/mgweb-server/config/mgweb.log /opt/qoper8 \
  && sed -i 's/x86_64/aarch64/g' /opt/qoper8/mgweb.conf.json \
  && cp /opt/mgweb-server/apache/apache2.conf /etc/apache2 \
  && cp /opt/mgweb-server/apache/ports.conf /etc/apache2 \
  && cp /opt/mgweb-server/apache/mpm_event.conf /opt/mgweb

RUN git clone https://github.com/chrisemunt/mgsi /opt/qoper8/mgsi \
  && cp /opt/qoper8/mgsi/yottadb/* /opt/qoper8/m \
  && yottadb -run ylink^%zmgsi \
  && cp /opt/qoper8/mgsi/unix/zmgsi.ci /usr/local/lib/yottadb/r138

RUN cp /opt/qoper8/mgsi/unix/zmgsi_ydb /usr/local/lib/yottadb/r138 \
  && cp /opt/qoper8/mgsi/unix/zmgsi_xinetd /etc/xinetd.d/zmgsi_xinetd \
  && cp /opt/qoper8/mgsi/unix/zmgsi.ci /usr/local/lib/yottadb/r138 \
  && sed -i 's/130/138/g' /etc/xinetd.d/zmgsi_xinetd \
  && sed -i 's/130/138/g' /usr/local/lib/yottadb/r138/zmgsi_ydb \
  && sed -i 's/1.30/1.38/g' /usr/local/lib/yottadb/r138/zmgsi_ydb \
  && echo "zmgsi_xinetd          7041/tcp                        # zmgsi" >> /etc/services \
  && rm -r /opt/qoper8/mgsi \
  && rm -r /tmp/tmp

RUN cd /opt/qoper8

RUN npm install nodem \
  && cp /opt/qoper8/node_modules/nodem/src/v4wNode.m /opt/qoper8/m


# Install Redis

RUN curl -fsSL https://packages.redis.io/gpg | gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/redis.list \
  && apt update \
  && apt install -y redis \
  && redis-server --daemonize yes \
  && update-rc.d redis-server defaults

RUN echo 'vm.overcommit_memory = 1' >> /etc/sysctl.conf


#RUN mkdir /redis \
#  && wget http://download.redis.io/redis-stable.tar.gz \
#  && tar xvzf redis-stable.tar.gz \
#  && mv redis-stable /redis \
#  && cd /redis \
#  && make \
#  && make install \

# Configure Redis

#RUN mkdir /etc/redis \
#  && mkdir /var/redis \
#  && cd /redis \
#  && cp utils/redis_init_script /etc/init.d/redis_6379 \
#  && cp redis.conf /etc/redis/6379.conf \
#  && mkdir /var/redis/6379 \
#  && sed -i 's/daemonize no/daemonize yes/g' redis.conf \
#  && sed -i 's[logfile ""[logfile /var/redis/6379/redis_6379.log[g' redis.conf \
#  && sed -i 's[dir ./[dir /var/redis/6379[g' redis.conf \
#  && update-rc.d redis_6379 defaults

# Clean up

EXPOSE 9080

RUN rm gde.txt

COPY ./index.html /opt/qoper8/index.html
COPY ./perf.css /opt/qoper8/perf.css
COPY ./img/YottaDB_logo-light.svg /opt/qoper8/img/YottaDB_logo-light.svg
COPY ./img/mgw_logo.latest.png /opt/qoper8/img/mgw_logo.latest.png
COPY ./img/fws-logo-horizontal.png /opt/qoper8/img/fws-logo-horizontal.png
COPY ./perf.js /opt/qoper8/perf.js
COPY ./fonts /opt/qoper8/fonts

ENTRYPOINT redis-server --daemonize yes && yottadb -r %XCMD 'do start^%zmgsi(0)' && yottadb -r %ydbwebreq
