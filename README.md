# Is YottaDB the fastest key-value database on the planet?

## Decide for yourself!

Code in this repo compares YottaDB performance against other databases. Clone this repository and build a Docker container in the cloned directory, e.g.,

```
git clone https://gitlab.com/YottaDB/Demo/performance-comparisons.git
cd performance-comparisons
```

Build and run a Docker image using the following command:

```
docker build -t perf .
docker run -p 9080:9080 --rm -it perf
```

Point your web browser at port 9080 on the machine hosting the Docker image, e.g., `http://localhost:9080`.

## Links

- [mg-dbx-napi](https://github.com/chrisemunt/mg-dbx-napi) is developed by [MGateway Ltd](https://www.mgateway.com/)
- [Nodem](https://github.com/dlwicksell/nodem) is developed by [Fourth Watch Software LC](https://fourthwatchsoftware.com/)
- [Redis](https://redis.io/) is developed by [Redis Ltd](https://redis.com/)
- [YottaDB](https://yottadb.com/resources/documentation/) is developed by [YottaDB LLC](https://yottadb.com/about-us/)

## Trademarks and Licenses

Ownership of trademarks by their holders is acknowledged. Software not released under the terms of another license is released under the terms of the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
