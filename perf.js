window.onload = async () => {
	populatePre('mgx', 'mgx-pre', '/node/mgdbx');
	populatePre('redis-par',   'redis-par-pre',   '/node/redis2');
	populatePre('redis-ser',   'redis-ser-pre',   '/node/redis1');
	populatePre('nodem',       'nodem-pre',       '/node/nodem');
	populatePre('mgxnw',       'mgxnw-pre',       '/node/mgdbx-nw');
}

function populatePre(buttonId, preId, endpoint) {
    document.getElementById(buttonId).addEventListener('click', () => {
        document.getElementById(preId).textContent = "fetching " + endpoint;

        fetch(endpoint).then(response => {
	    if (response.ok) {
		    return response.text();
	    }
	    else {
		    return "error from web service call"
	    }
        }).then(data => {
            document.getElementById(preId).textContent = data;
        })
    })
}
