foo
        ; YottaDB + node.js + mg-dbx-napi: node benchmark_mgdbxnapi.mjs
        ; YottaDB + bun + mg-dbx-napi: bun benchmark_mgdbxnapi.mjs
        ; YottaDB + node.js +nodem: node benchmark_nodem.js
        ; Redis (serial) + node.js: node benchmark_redis.mjs
        ; Redis (pipelined) + node.js: node benchmark_redis_pl.mjs
mgdbx
        open "mgdbx":(shell="/bin/bash":command="node benchmarks/benchmark_mgdbxnapi.mjs"):5:"pipe"
        use "mgdbx"
        for i=1:1 read httprsp(i)  set httprsp(i)=httprsp(i)_$c(10) quit:$zeof
        close "mgdbx"
        set httprsp("mime")="text/plain"
        quit
        ;
mgdbxnw
        open "mgdbxnw":(shell="/bin/bash":command="node benchmarks/benchmark_mgdbxnapi_nw.mjs"):5:"pipe"
        use "mgdbxnw"
        for i=1:1 read httprsp(i)  set httprsp(i)=httprsp(i)_$c(10) quit:$zeof
        close "mgdbxnw"
        set httprsp("mime")="text/plain"
        quit
        ;
redis1
        open "redis":(shell="/bin/bash":command="node benchmarks/benchmark_redis.mjs"):5:"pipe"
        use "redis"
        for i=1:1 read httprsp(i)  set httprsp(i)=httprsp(i)_$c(10) quit:$zeof
        close "redis"
        set httprsp("mime")="text/plain"
        quit
        ;
redis2
        open "redis":(shell="/bin/bash":command="node benchmarks/benchmark_redis_pl.mjs"):5:"pipe"
        use "redis"
        for i=1:1 read httprsp(i)  set httprsp(i)=httprsp(i)_$c(10) quit:$zeof
        close "redis"
        set httprsp("mime")="text/plain"
        quit
        ;
nodem
        open "nodem":(shell="/bin/bash":command="node benchmarks/benchmark_nodem.js"):5:"pipe"
        use "nodem"
        for i=1:1 read httprsp(i)  set httprsp(i)=httprsp(i)_$c(10) quit:$zeof
        close "nodem"
        set httprsp("mime")="text/plain"
        quit
        ;
